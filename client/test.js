import React, { Component } from "react";
import UserDataService from "../../services/user.service"
import Table from 'react-bootstrap/Table';
import CardDeck from 'react-bootstrap/CardDeck'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import Dropdown from 'react-bootstrap/Dropdown'
import Form from 'react-bootstrap/Form';



import Image from 'react-bootstrap/Image'
import Jumbotron from 'react-bootstrap/Jumbotron'
class Hotels extends Component{
    constructor(props) {
        super(props);
        this.handleRoomData=this.handleRoomData.bind(this)
        this.handleGuests=this.handleGuests.bind(this)


        this.state = {
          image:'',
          name:'',
          selectValue:null,
          numberOfAdults:'',
          isLoggedIn: false,
          isSelected:false

        }
      }

      handleGuests(event){
        this.setState({numberOfAdults:event.currentTarget.value})  
       }
    
      handleRoomData(event){ 
        this.setState({
          selectValue: event.currentTarget.value,
          isSelected:true
        })
                     
    }
  
      componentWillUpdate(){
        const {state} = this.props.location;
        if(state){
          this.setState({
            image: state.image, 
            name:state.name
        });
        }
      }
      
      render() {
        let {data}=this.props.location;
        var message='You selected '+this.state.isSelected;

        let {isLoggedIn} = this.state.isSelected;

        const renderAuthButton = ()=>{
          if(isLoggedIn){
            return <Container>Logout</Container>
          } else{
            return <button>Login</button>
          }
        }
        
        return (
            <React.Fragment>
              <div className='container'>
              {renderAuthButton()}

              </div>
          <Jumbotron style={{height:'10%',margin:'8%'}} fluid>
          
                <Row>
                  <Col><Image  style={{margin:'5%'}} src='https://www.travelodge.co.uk/sites/default/files/styles/c12/public/GB0135_Edinburgh_Central_Princes_St_Exterior_2208x1656.jpg' fluid /></Col>
                  <Col style={{justifyContent: "center",alignItems: "center"}}>
                  
                  <h3 style={{margin:'5%'}}>Edinburgh Central Princes Street</h3>  
                  <h6 style={{color:'gray'}}>1Meuse Lane, off Princes Street,Edinburgh, EH2 2BY, United Kingdom</h6> 
                  <p style={{color:'#78a6b3',marginTop:'8%',fontSize:'1.2rem' ,float: "left",marginLeft:'15%',marginRight:'15%'}}>Prime central location and only a short stroll from the train station</p>                          
                  <Badge style={{fontSize:'1.2rem'}} variant="info">Price </Badge>{' '}     
                  <Card  style={{fontSize:'1.2rem',margin:'5%'}}>
                    <Card.Header>Check Availability </Card.Header>
                    <Card.Body>
                      <Card.Title style={{fontSize:'small'}}></Card.Title>
                      <Card.Text>
                        <Row>
                          <Col>
                          <Form.Group controlId="exampleForm.SelectCustom">
                            <Form.Label as='h7'>Rooms</Form.Label>
                            <Form.Control as="select"  size="sm" custom onChange={this.handleRoomData} value={this.state.selectValue}>
                            <option> </option>
                            <option >1</option>
                            <option >2</option>
                            <option >3</option>
                            <option >4</option>
                            </Form.Control>
                        </Form.Group>
                          </Col>
                          <Col>
                          <Form.Group controlId="exampleForm.SelectCustom">
                            <Form.Label as='h7'>Adults</Form.Label>
                            <Form.Control as="select"  size="sm" custom onChange={this.handleGuests} value={this.state.numberOfAdults} defaultValue='0'>
                            <option> </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            </Form.Control>
                        </Form.Group>
                          </Col>
                        </Row>
                      </Card.Text>
                      <Button variant="primary">Book Now</Button>
                    </Card.Body>
                    {message}
                  </Card>
                  </Col>
                  <Col>
                <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Card.Link href="#">Card Link</Card.Link>
    <Card.Link href="#">Another Link</Card.Link>
  </Card.Body>
</Card>

<Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Card.Link href="#">Card Link</Card.Link>
    <Card.Link href="#">Another Link</Card.Link>
  </Card.Body>
</Card>
                  </Col> 
                </Row>   
                
                      
          </Jumbotron>
            </React.Fragment>
            
        );
      }
}

export default Hotels;