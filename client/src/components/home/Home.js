import React, { Component } from "react";
import Carousel from 'react-bootstrap/Carousel'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";;
import Badge from 'react-bootstrap/Badge'
import Jumbotron from 'react-bootstrap/Jumbotron'


import HotelDataService from "../../services/hotel.service"

import Hotels from './Hotels'

class Home extends Component {
    
    constructor() {
        super();
        
       
        this.handleCheckInDate=this.handleCheckInDate.bind(this)
        this.handleCheckOutDate=this.handleCheckOutDate.bind(this)
        this.handleSearchData=this.handleSearchData.bind(this)
        this.onChange=this.onChange.bind(this);
        this.test=this.test.bind(this);

        this.searchedHotel=this.searchedHotel.bind(this)

        this.state = {
           
            index: 0,
            direction: null,
            check_in_date: new Date(),
            check_out_date: new Date(),
            location:'',
            hotels: [],
        };
      }
      
    
    handleCheckInDate = date => {
        this.setState({
          check_in_date: date,
        });
      };

      handleCheckOutDate = date =>{
        this.setState({
          check_out_date:date,
        });
      }

      onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
      };
  
      componentDidMount() {
        this.retrieveHotels();
        const isuserAuthenticated = localStorage.getItem('jwtToken');
        console.log(isuserAuthenticated,'issssss');

        if(!isuserAuthenticated){
        
          this.props.history.push("/login");
        }  
      }

      handleSearchData(event){
          
        this.setState({location: event.currentTarget.value});
        
    }
    
     //Retrieve all Hotel data
      retrieveHotels() {
        HotelDataService.getAll()
          .then(response => {
            this.setState({
              hotels: response.data
            });
          })
          .catch(e => {
            console.log(e);
          });
      }

      //Retrieve Searched Hotels
      searchedHotel(e){
        e.preventDefault();

        function convert(str) {
          var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
          return [date.getFullYear(), mnth, day].join("-");
        }
 
        const Data = {
          location: this.state.location,
          check_in_date: convert(this.state.check_in_date),
          check_out_date:convert(this.state.check_out_date)
        };
        
        HotelDataService.getSearchedData(Data)
          .then(response => {
            this.setState({
              hotels: response.data
            });
            console.log(response.data,'response');
          })
          .catch(e => {
            console.log(e);
          });

      }

      //Render Student data 
      renderTableData() {
        return this.state.hotels.map((filteredPerson,index)=>{
          const { id ,name, location, reviews,description } = filteredPerson ;
          return (
              <tr key={id }>
                 <td>{name}</td>
                 <td>{location}</td>
                 <td>{reviews}</td>
                 <td>{description}</td>
              </tr>
           )
        })
      
   }

   test(){
    this.props.history.push('/register','abc');
    
   }
      
    render(){
        const settings = {
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed:500
          };
          

          const { index, direction,check_in_date ,check_out_date} = this.state;
        
        
        return(
            <React.Fragment>
               
             <Carousel>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://picsum.photos/800/400?text=First slide&bg=373940"
						alt="First slide"
					/>
					<Carousel.Caption>
                    
						<h3 style={{fontFamily:'font-family: Trebuchet MS, sans-serif'}}>Coronavirus (COVID-19) Update</h3>
						<h5 style={{fontFamily:'font-family: Trebuchet MS, sans-serif'}} >You can amend your saver rate bookings without paying a fee for all stays until further notice, subject to conditions and availability.</h5>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://picsum.photos/800/400?text=Second slide&bg=282c34"
						alt="Third slide"
					/>
					<Carousel.Caption>
						<h3>Inspiring short breaks</h3>
						<h5>Spring has sprung and with better weather on the horizon, there couldn't be a better time to get out and about. Whether it's a walk through a stunning national park, enjoying that extra sunlight in the evening, or rummaging around on an easter egg hunt there's plenty to do across the UK this spring..</h5>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://picsum.photos/800/400?text=Third slide&bg=20232a"
						alt="Third slide"
					/>
                <Carousel.Caption>
						<h3>Great for business stays</h3>
						<h5>
                        Whether you're staying over before an early morning meeting or just cutting down your commute, Travelodge is a great place for business travellers. With speedy Wi-Fi, our signature king size Travelodge Dreamer™ bed and over 190 hotels offering a bumper breakfast to kick start your day, Travelodge really is 'best for business'.
                        </h5>
					</Carousel.Caption>
				</Carousel.Item>
                <Carousel.Item>
					<img
						className="d-block w-100"
						src="https://picsum.photos/800/400?text=Fourth slide&bg=20232a"
						alt="Fourth slide"
					/>
		        <Carousel.Caption>
						<h3>Group getaways</h3>
						<h5>
                        Planning ahead for this spring's wedding season? Whether you’re arranging group accommodation for a wedding party, family party, corporate event, or sports tour, Travelodge is the ideal choice for groups booking 10 rooms or more.
                    </h5>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>
            <Container className='card-container'>
            <Card  border="primary">
                <Card.Header as="h5">Where would you like to go?</Card.Header>
                <Card.Body>
                
                <Row>
                    <Col>
                    <Form.Group controlId="exampleForm.SelectCustom" style={{margin:'2%'}}>
                            <Form.Label as='h7'></Form.Label>
                            <Form.Control as="select"  size="sm" custom onChange={this.handleSearchData} value={this.state.location}>
                            <option>London</option>
                            <option>Edinburgh</option>
                            <option>Cardiff</option>
                            <option>Manchester</option>
                            <option>Cambridge</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                   
                </Row>
                
                <Row>
                    <Col>
                    <h7 style={{margin:'8%',fontSize:'0.8rem'}}> Check In Date</h7>
                      <Form.Group controlId="exampleForm.SelectCustom">
                          
                        <DatePicker style={{margin:'2%'}} 
                            placeholderText="Click to select a date" 
                            showPopperArrow={false}
                            selected={check_in_date}
                            onChange={this.handleCheckInDate}
                            minDate={new Date()}
                            showDisabledMonthNavigation/>
                      </Form.Group>
                    
                    </Col>
                    <Col>
                    <h7 style={{margin:'8%',fontSize:'0.8rem'}}> Check Out Date</h7>
                    <Form.Group controlId="exampleForm.SelectCustom">

                    <DatePicker 
                                      placeholderText="Click to select a date" 
                                      showPopperArrow={false}
                                      selected={check_out_date}
                                      onChange={this.handleCheckOutDate}
                                      minDate={new Date()}
                                      showDisabledMonthNavigation/>
                    </Form.Group>
                          
        </Col>

                </Row>
                    <Button variant="primary" onClick={this.searchedHotel}>Search</Button>
                </Card.Body>
                </Card>
            </Container>

<Row style={{display:'flex',justifyContent:'center',alignItems:'center',height:'95%',width:'100%',float:'left'}}>
  
  {this.state.hotels.map( data => { 
        return <Card key={data.id} style={{margin:'2%',float:'left',width:'26%',boxShadow:'0px 14px 80px rgba(34, 35, 58, 0.2)'}} border="primary">

        <Card.Img variant="top" src={data.image} />
        <Card.Body>
            <Card.Title><Card.Link variant='dark'onClick={()=>{ 
              const hotel=data

               this.props.history.push('/hotel',hotel);
            }} href="#">{data.name}</Card.Link></Card.Title>
          <Card.Text style={{fontSize:'medium',color:'gray'}}>
            {data.address}
          </Card.Text>
          <Card.Text style={{fontSize:'small',color:'red',fontStyle:'bold'}} href="#">From : £ {data.price} </Card.Text>
        </Card.Body>
        <Card.Footer>
            <Badge pill variant="info">
                  Ratings :{data.reviews}
            </Badge>{' '}
            
        </Card.Footer>
      </Card>
      
    })}
</Row>     
            </React.Fragment>
            
        )
        
        
    }
}

export default Home;