import React, { Component } from "react";
import UserDataService from "../../services/user.service"
import Table from 'react-bootstrap/Table';
import CardDeck from 'react-bootstrap/CardDeck'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import Dropdown from 'react-bootstrap/Dropdown'
import Form from 'react-bootstrap/Form';



import Image from 'react-bootstrap/Image'
import Jumbotron from 'react-bootstrap/Jumbotron'
class Hotels extends Component{
    constructor(props) {
        super(props);
        this.handleRoomData=this.handleRoomData.bind(this)
        this.handleGuests=this.handleGuests.bind(this)


        this.state = {
          image:'',
          name:'',
          adrress:'',
          description:'',
          selectValue:null,
          numberOfAdults:'',
          price:'',
          
          isSelected:false,
          roomData:[]
        

        }
      }

      handleGuests(event){
        this.setState({numberOfAdults:event.currentTarget.value})  
       }
    
      handleRoomData(event){ 
        this.setState({
          selectValue: event.currentTarget.value,
          isSelected:true,
          
          
        })
                     
    }
  
      componentDidMount(){
        const {state} = this.props.location;
        if(state){
          this.setState({
            image: state.image, 
            name:state.name,
            roomData:state.rooms,
            address:state.address,
            description:state.description,
            price:state.price

        });
        }
      }
      
      render() {
        
        
        
        return (
            <React.Fragment>
          <Jumbotron style={{height:'100%'}} fluid>
          
                <Row>
                  <Col><Image  style={{margin:'5%'}} src={this.state.image} fluid /></Col>
                  <Col style={{justifyContent: "center",alignItems: "center"}}>
                  
        <h3 style={{margin:'5%'}}>{this.state.name}</h3>  
        <h6 style={{color:'gray'}}>{this.state.address}</h6> 
        <p style={{color:'#78a6b3',marginTop:'8%',fontSize:'1.2rem' ,float: "left",marginLeft:'15%',marginRight:'15%'}}>{this.state.description}</p>   
        
            <Badge pill variant="warning" style={{fontSize:'1.2rem'}}>
              From : £ {this.state.price}
            </Badge>{' '}                     
                
                  </Col>
     
                </Row>  
              
                <div style={{ width: '100%',display:'flex',justifyContent:'center',alignItems:'center',flexDirection:'column'}}> 
                {this.state.roomData.map( data => { 
                return<Card border='danger' key={data.id}fluid style={{margin:'2%',boxShadow:'0px 14px 80px rgba(34, 35, 58, 0.2)'}}>
                  <Card.Header as="h5">{data.name}</Card.Header>
                  <Card.Body>
                  <div style={{ height:'20vh',width: '100%',float:'left',display:'flex',alignItems:'center'}}>
                    <div style={{width:'30%',height:'100%',}}>
                    <Image  style={{margin:'2%'}} src={data.image} fluid />
                        
                    </div>
                    <div style={{width:'40%',height:'100%',alignItems:'left'}}>
                    
                    <div style={{width:'100%',height:'20%'}}>
            
                    </div>
                      <div style={{width:'100%',height:'80%'}}>
                    
                    <ListGroup style={{margin:'5%'}}>
                      {data.facilities.map(service=>{
                        return <ListGroup.Item  action variant="success"style={{fontSize:'0.8rem'}}>{service}</ListGroup.Item>
                      })}
                    </ListGroup>
                    </div>       

                    </div>
                    <div style={{width:'20%',height:'100%'}}>
                          <div style={{width:'100%',height:'50%'}}>
                          <Form.Group controlId="exampleForm.SelectCustom" style={{margin:'2%'}}>
                            <Form.Label as='h6'>Rooms</Form.Label>
                            <Form.Control as="select"  size="sm" custom onChange={this.handleSearchData} value={this.state.location}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </Form.Control>
                        </Form.Group>
                          </div>    
                          <div style={{width:'100%',height:'50%',backgroundColor:"black"}} style={{margin:'2%'}}>
                            {/*     //users num */}
                            <Form.Group controlId="exampleForm.SelectCustom">
                              <Form.Label as='h6'>Adults</Form.Label>
                              <Form.Control as="select"  size="sm" custom onChange={this.handleSearchData} value={this.state.location}>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                              <option>6</option>
                              <option>7</option>
                              </Form.Control>
                          </Form.Group>
                          </div> 
                    </div>
                    <div style={{width:'20%',height:'100%'}}>
                        {/* book now btn*/}
                        <div style={{margin:'12%'}}>
                          <Button variant="success" onClick={()=>{ 
              const hotel=data

               this.props.history.push('/book',hotel);
            }} href="#">Book Now</Button>
                        </div>
                    </div>
                </div>
                  </Card.Body>
                  <Card.Footer className="text-muted" style={{margin:'2%',fontSize:'0.8rem',backgroundColor:'yellow',color:'black'}}>Flexible Room bookings can be canceled up until noon on the date of arrival for a full refund</Card.Footer>
                </Card>
                })}
          </div>  
                
                      
          </Jumbotron>
            </React.Fragment>
            
        );
      }
}

export default Hotels;