import React, { Component } from "react";
import { Link } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import setAuthToken from '../../utils/setAuthToken'
import jwt_decode from "jwt-decode";
import UserDataService from "../../services/user.service"

class Login extends Component {
  constructor(props) {
    super(props);
    this.onSubmit=this.onSubmit.bind(this);

    this.state = {
      email: "",
      password: "",
      errors: {},
      setCurrentUser:"",
      isAuthenticated: false,
      user: {},
      loading: false
    };
  }

onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

onSubmit = e => {
    e.preventDefault();

const userData = {
      email: this.state.email,
      password: this.state.password
    };

UserDataService.login(userData,this.props.history).then(
  response=>{
    
    //set token in localstorage
    const { token } = response.data;
    localStorage.setItem("jwtToken", token);
    // Set token to Auth header
    setAuthToken(token);
    // Decode token to get user data
    const decoded = jwt_decode(token);
    // Set current user
    this.setState({
      setCurrentUser:decoded
    })
   
    console.log(this.state.setCurrentUser,'currenttttt')

    this.props.history.push("/home")
  }).catch(err=>{
    this.setState({
      errors:err.response.data


    })
  })
  };

  logOutUser(){
    // Remove token from local storage
  localStorage.removeItem("jwtToken");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false

  this.setState({
    setCurrentUser:{},
    isAuthenticated:false
  })

  }


render() {
    const { errors } = this.state;
return (
      <div className="container">
        <div style={{ marginTop: "4rem" }} className="row">
          <div className="col s8 offset-s2">
            <Link to="/" className="btn-flat waves-effect">
              <i className="material-icons left"></i> Back to
              home
            </Link>
            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
              <h4>
                <b>Sign in</b>
              </h4>

              <h5>You can sign in using your Travel Lodge account to access our services.</h5>
              
            </div>
            <div className='col 12' style={{paddingLeft:"40.250px" , paddingRight:"40.250px"}}>

<Form>
<Form.Group controlId="email" value={this.state.email}><span className="red-text">
                  {errors.email}
                  {errors.emailnotfound}
                </span>
    <Form.Control size="sm" type="email" name='email' placeholder="Email" onChange={this.onChange} error={errors.email} />
    <Form.Text className="text-muted">
    </Form.Text>
</Form.Group>

<Form.Group controlId="password" value={this.state.password}><span className="red-text">
                  {errors.password}
                  {errors.passwordincorrect}
                </span>
    <Form.Control size="sm" type="password" name='password' placeholder="Password" onChange={this.onChange} error={errors.password}/>
</Form.Group>

<Button variant="info" type="submit" className="btn btn-large waves-effect waves-light hoverable blue accent-3" onClick={this.onSubmit}> 
Sign In
</Button>
</Form>
<p className="grey-text text-darken-1">
                Don't have an account? <Link to="/register">Sign Up</Link>
              </p>
</div>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;