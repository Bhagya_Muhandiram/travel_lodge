import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Router, Route } from "react-router-dom";


import  Landing  from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import logout from'./components/auth/Logout';
import Home from "./components/home/Home";
import Hotels from "./components/home/Hotels"
import Footer from "./components/layout/Footer";
import NavBar from './components/layout/Navbar';
import Help from "./components/home/Help"
import Bookings from './components/home/Bookings';


import  history  from './history';

import './App.css';



const PrivateRoute = ({ component: Component, ...rest }) => {

  const [auth, setAuth] = useState(false);
  const [isTokenValidated, setIsTokenValidated] = useState(false);

  useEffect(() => {
    // send jwt to API to see if it's valid
    let token = localStorage.getItem("token");
    if (token) {
      fetch("/protected", {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ token })
      })
      .then((res) => {
        return res.json()
      })
      .then((json) => {
        if (json.success) {
          setAuth(true);
        }
      })
      .catch((err) => {
        setAuth(false);
        localStorage.removeItem("token");
      })
      .then(() => setIsTokenValidated(true));
    } else {
       setIsTokenValidated(true); // in case there is no token
    }

  }, [])

 if (!isTokenValidated) return <div />; // or some kind of loading animation

  return (<Route {...rest}
    render={(props) => {
      return auth ? <Component {...props} /> : <Redirect to="/login" />
    }} />)
  }


function App() {
  return (
    <Router history={history}>
<div className="App">
    <NavBar/>   
    <Route exact path="/" component={Login} />
    <Route exact path="/register" component={Register} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/home" component={Home} />
    <Route exact path="/hotel" component={Hotels} />
    <Route exact path="/book" component={Bookings} />

</div>
    
    </Router>
      
  );
}

export default App;
