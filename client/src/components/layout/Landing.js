import React, { Component } from "react";
import Button from 'react-bootstrap/Button';


class Landing extends Component {
  render() {
    return (
      <div style={{ height: "60vh" }} className="container valign-wrapper">
        
          <div className="col s12 center-align">
            <h4>
              <b>Welcome!</b>
            </h4>
            <p className="flow-text grey-text text-darken-1">
              Create a (minimal) full-stack app with user authentication via
              passport and JWTs
            </p>
   <Button variant="info" href='/login' className="btn btn-large waves-effect waves-light hoverable blue accent-3" style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}>Log In</Button>{' '}
  <Button variant="info" href='/register' className="btn btn-large waves-effect waves-light hoverable blue accent-3" style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}>Register</Button>{' '} 
        </div>
      </div>
    );
  }
}
export default Landing;