import React, { Component } from "react";
import UserDataService from "../../services/user.service"
import Table from 'react-bootstrap/Table';
import CardDeck from 'react-bootstrap/CardDeck'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import Dropdown from 'react-bootstrap/Dropdown'
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup'
import Nav from 'react-bootstrap/Nav'



import Image from 'react-bootstrap/Image'
import Jumbotron from 'react-bootstrap/Jumbotron'
class Bookings extends Component{
    constructor(props) {
        super(props);
        


        this.state = {
          image:'',
          name:'',
          adrress:'',
          description:'',
          selectValue:null,
          numberOfAdults:'',
          
          isSelected:false,
          roomData:[]
        

        }
      }

      componentDidMount(){
        const {state} = this.props.location;
        console.log(state,'statteeeeeee')
        
        if(state){
          this.setState({
            image: state.image, 
            name:state.name,
            roomData:state.rooms,
            address:state.address,
            description:state.description

        });
        }
      }
      
      render() {
        
        
        
        return (
            <React.Fragment>
                <div className='container'>
                    
                


<ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
		<i class="fa fa-credit-card"></i> Credit Card</a></li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="pill" href="#nav-tab-bhagi">
		<i class="fab fa-paypal"></i>  Paypal</a></li>
	
</ul>

<div class="tab-content">
<div class="tab-pane fade show active" id="nav-tab-card">
	
	<form role="form">
	<div class="form-group">
		<h6 for="username" style={{margin:'2%',fontSize:'1.0rem'}}>Full name ( on the card)</h6>
		<input type="text" class="form-control" name="username" placeholder="" required=""/>
	</div> 

	<div class="form-group">
		<h6 for="cardNumber" style={{margin:'2%',fontSize:'1.0rem'}}>Card number</h6>
		<div class="input-group">
			<input type="text" class="form-control" name="cardNumber" placeholder=""/>
			<div class="input-group-append">
				<span class="input-group-text text-muted">
					<i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>   
					<i class="fab fa-cc-mastercard"></i> 
				</span>
			</div>
		</div>
	</div> 

	<div class="row">
	    <div class="col-sm-8">
	        <div class="form-group">
	            <h7 style={{margin:'2%'}}><span class="hidden-xs">Expiration</span> </h7>
	        	<div class="input-group">
	        		<input type="number" class="form-control" placeholder="MM" name=""/>
		            <input type="number" class="form-control" placeholder="YY" name=""/>
	        	</div>
	        </div>
	    </div>
	    <div class="col-sm-4">
	        <div class="form-group">
	            <label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
	            <input type="number" class="form-control" required=""/>
	        </div> 
	    </div>
	</div> 
	<button class="subscribe btn btn-primary btn-block" type="button"> Confirm  </button>
	</form>
</div> 
<div class="tab-pane fade" id="nav-tab-bhagi">
<p>Paypal is easiest way to pay online</p>
<p>
<button type="button" class="btn btn-primary"> <i class="fab fa-paypal"></i> Log in my Paypal </button>
</p>
<p><strong>Note:</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. </p>
</div>

</div> 

                </div>
    
         
            </React.Fragment>
            
        );
      }
}

export default Bookings;