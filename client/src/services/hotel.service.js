import http from "../http-common";

class HotelDataService {
  
    //Get all available hotels
    getAll() {
      return http.get("/hotels");
    }

    getSearchedData(data){
        return http.post("/test",data)
    }
  
    get(id) {
      return http.get(`/test/${id}`);
    }
  
    update(id, data) {
      return http.put(`/test/${id}`, data);
    }
  
    delete(id) {
      return http.delete(`/test/${id}`);
    }
  
    deleteAll() {
      return http.delete(`/test`);
    }
  
    
  }
  
  export default new HotelDataService();