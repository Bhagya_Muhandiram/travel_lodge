import React, { Component } from "react";
import { BrowserRouter as Router} from "react-router-dom";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";


class NavBar extends Component {
  render() {
    return (
      <Router>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" >
          <Navbar.Brand style={{fontFamily:'Trebuchet MS, sans-serif'}} href={'/home'}>Travel Lodge</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              
            </Nav>
            <Nav>
              <Nav.Link href={'/'}>Bookings</Nav.Link>
              <Nav.Link href={'/'}>Contact Us</Nav.Link>
              <Nav.Link href={'/help'}>Help</Nav.Link>
              
            </Nav>
          </Navbar.Collapse>
          </Navbar>
          </Router>
    );
  }
}
export default NavBar;